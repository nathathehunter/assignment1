Nathan Caldwell - 8659706
Fixes

Percievable
Text Alternatives
    A label has been added for the image describing its content, and an alt has been given within its tag
Time Based Media
    A description of what happens when the button is pressed has been added, along with captions of what is said within the file
Distinguishable
    The background colour has been changed to basic white, making the black text easily readable
Operable
Navigable
    The page has been given a title tag naming it as "Accessible Page" so the user knows what the page is about
    Headers have been added where necessary to separate them from content
Understandable
Input Assistance
    The button now says exactly what it does, and has a new accom[anying label
    The radio buttons have their labels linked to them, assisting screen readers
